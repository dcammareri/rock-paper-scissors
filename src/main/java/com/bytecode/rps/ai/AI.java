package com.bytecode.rps.ai;

import java.io.Serializable;

import static com.bytecode.rps.engine.GameEngine.Gesture;
import static com.bytecode.rps.engine.GameEngine.Result;

/**
 * Contains the logic to be used by the application to choose a move to play.
 */
public interface AI extends Serializable {

    /**
     * Pick out a gesture to be played.
     *
     * @param lastOpponentMove The previuos gesture played by the opponent.
     *                         Should be used for guessing or for keeping
     *                         historical data
     * @param lastResult       The previous result.
     *                         Should be used for guessing or for keeping
     *                         historical data
     * @return A Gesture (ROCK, PAPER or SCISSORS)
     */
    Gesture nextMove(Gesture lastOpponentMove, Result lastResult);

    /**
     * Reset the data history of the AI, if any.
     */
    void reset();
}
