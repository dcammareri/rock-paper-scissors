(function ($) {
    var ractive;
    var gestures;
    $.ajax('/template.html').then(function (template) {
        ractive = new Ractive({
            el: 'app',
            template: template,
            data: {player1Wins: 0, ties: 0, player2Wins: 0}
        });
        //for performance reason, execute selector once
        gestures = $("#gestures button")
        gestures.on("click", function () {
                $("#gestures button").attr("disabled", "disabled");
                $.ajax('/ajax/play?gesture=' + this.id).then(function (data) {
                    ractive.set({error: false})
                    ractive.set(data);
                    if (data.lastResult == 'PLAYER1WIN') {
                        ractive.set({won: true, tie: false, lost: false});
                    } else if (data.lastResult == 'PLAYER2WIN') {
                        ractive.set({won: false, tie: false, lost: true});
                    } else {
                        ractive.set({won: false, tie: true, lost: false});
                    }
                    gestures.removeAttr("disabled");
                }, function () {
                    ractive.set({error: true, won: false, tie: false, lost: false})
                    gestures.removeAttr("disabled");
                })
            }
        );

        $("#reset").on("click", function () {
            $.ajax('/ajax/reset').then(function (data) {
                ractive.set(data);
                ractive.set({won: false, tie: false, lost: false});
            })
        })

        $.ajax('/ajax/current').then(function (data) {
            ractive.set(data);
        })
    });

}(jQuery));

