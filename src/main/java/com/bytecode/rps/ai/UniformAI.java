package com.bytecode.rps.ai;

import com.bytecode.rps.engine.GameEngine;

import java.util.Random;

import static com.bytecode.rps.engine.GameEngine.Gesture;

/**
 * An AI Implementation that pick all the gestures with equal probability.
 */

public class UniformAI implements AI {

    private Random random = new Random(System.currentTimeMillis());

    @Override
    public Gesture nextMove(Gesture lastOpponentMove, GameEngine.Result lastResult) {
        Gesture[] gestures = Gesture.values();
        return gestures[random.nextInt(gestures.length)];
    }

    @Override
    public void reset() {

    }
}
