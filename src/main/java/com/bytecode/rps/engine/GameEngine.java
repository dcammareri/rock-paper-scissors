package com.bytecode.rps.engine;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

import static com.bytecode.rps.engine.GameEngine.Gesture.*;
import static com.bytecode.rps.engine.GameEngine.Result.*;

/**
 * Holds the logic of Rock Paper Scissors game.
 *
 * @see <a href="http://en.wikipedia.org/wiki/Rock-paper-scissors">Rock Paper Scissors on wikipedia</a>
 */
public abstract class GameEngine {

    private static final Logger log = LoggerFactory.getLogger(GameEngine.class);

    /**
     * Holds all the combinations in a map to be retrieved to check the result.
     * This provides instant access time and is more readable than a switch-case or if block.
     */
    private static Map<Pair<Gesture, Gesture>, Result> results = new HashMap<Pair<Gesture, Gesture>, Result>();

    static {
        results.put(new ImmutablePair<Gesture, Gesture>(ROCK, ROCK), TIE);
        results.put(new ImmutablePair<Gesture, Gesture>(ROCK, PAPER), PLAYER2WIN);
        results.put(new ImmutablePair<Gesture, Gesture>(ROCK, SCISSORS), PLAYER1WIN);

        results.put(new ImmutablePair<Gesture, Gesture>(PAPER, PAPER), TIE);
        results.put(new ImmutablePair<Gesture, Gesture>(PAPER, SCISSORS), PLAYER2WIN);
        results.put(new ImmutablePair<Gesture, Gesture>(PAPER, ROCK), PLAYER1WIN);

        results.put(new ImmutablePair<Gesture, Gesture>(SCISSORS, SCISSORS), TIE);
        results.put(new ImmutablePair<Gesture, Gesture>(SCISSORS, ROCK), PLAYER2WIN);
        results.put(new ImmutablePair<Gesture, Gesture>(SCISSORS, PAPER), PLAYER1WIN);
    }

    /**
     * Given 2 gestures, calculates the result of the play.
     *
     * @param player1 The gesture of first player
     * @param player2 The gesture of second player
     * @return <code>PLAYER1WIN</code> if the first player wins, <code>PLAYER2WIN</code> if the second player wins, <code>TIE</code> if it is a tie
     */
    public static Result play(Gesture player1, Gesture player2) {
        log.debug("Calculating the winner between {} and {}", player1, player2);

        if (player1 == null || player2 == null) {
            throw new IllegalArgumentException("One or more Gestures are null");
        }
        Result result = results.get(new ImmutablePair<Gesture, Gesture>(player1, player2));

        log.debug("Calculated the result between {} and {}. Result is: {}", player1, player2, result);
        return result;
    }

    /**
     * All the possible moves for a player.
     */
    public enum Gesture {
        ROCK, PAPER, SCISSORS;

        /**
         * Return the gesture that wins against itself.
         *
         * @return The Gesture to play to win
         */
        public Gesture theWinning() {

            Gesture winning = null;

            switch (this) {
                case ROCK:
                    winning = PAPER;
                    break;
                case PAPER:
                    winning = SCISSORS;
                    break;
                case SCISSORS:
                    winning = ROCK;
                    break;
                default:
                    log.warn("Gesture not handled: {}", this);
                    break;
            }

            return winning;
        }
    }

    /**
     * All the possible results.
     */
    public enum Result {
        PLAYER1WIN, TIE, PLAYER2WIN
    }


}
