package com.bytecode.rps.ai

import static com.bytecode.rps.engine.GameEngine.Gesture.*
import static com.bytecode.rps.engine.GameEngine.Result.*

import spock.lang.Specification


/**
 * Created by daniele on 28/06/14.
 */
class AITest extends Specification {

    private final static AIs = ['BiasedAI', 'DummyAI', 'UniformAI', 'PredictingAI']

    def "test NextMove just return a move"() {
        given:
        def ai = (AI)Class.forName("com.bytecode.rps.ai.$a").newInstance()

        when:
        def gesture = ai.nextMove(b,c)

        then:
        gesture in [ROCK,PAPER,SCISSORS]

        where:
        [a,b,c] << [AIs, [null, ROCK], [null, PLAYER1WIN]].combinations()
    }

    def "reset should work"() {
        given:
        def ai = (AI)Class.forName("com.bytecode.rps.ai.$a").newInstance()

        expect:
        ai.reset()

        where:
        a << AIs

    }

}
