package com.bytecode.rps.ai;

import com.bytecode.rps.util.ArrayUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import static com.bytecode.rps.engine.GameEngine.Gesture;
import static com.bytecode.rps.engine.GameEngine.Result;

/**
 * An AI Implementation that remember what the player has played based on last move and last result.
 */

public class PredictingAI implements AI {

    /**
     * A map that contains, for every move of the player and corresponding result, how many times he played each gesture the next time.
     *
     * The key is a pair of Gesture and Result. The value is a map that, for every gesture, contains the number of times that gesture has been played
     *
     */
    private Map<Pair<Gesture, Result>, Map<Gesture, Integer>> history;

    private Gesture secondLastOpponentMove;
    private Result secondLastResult;

    private Random random = new Random(System.currentTimeMillis());

    public PredictingAI() {
        initHistory();
    }

    private void initHistory() {
        history = new HashMap<Pair<Gesture, Result>, Map<Gesture, Integer>>();
        for (Gesture gesture : Gesture.values()) {
            for (Result result : Result.values()) {
                ImmutablePair<Gesture, Result> key = new ImmutablePair<Gesture, Result>(gesture, result);
                Map<Gesture, Integer> value = new HashMap<Gesture, Integer>();
                Gesture[] gestures = Gesture.values();
                ArrayUtils.shuffleArray(gestures); // to avoid picking always the same choice at the first run
                for (Gesture innerGesture : gestures) {
                    value.put(innerGesture, 0);
                }
                history.put(key, value);
            }
        }
    }

    @Override
    public Gesture nextMove(Gesture lastOpponentMove, Result lastResult) {

        updateHistory(lastOpponentMove);

        Map<Gesture, Integer> historyEntry = history.get(new ImmutablePair<Gesture, Result>(lastOpponentMove, lastResult));
        Gesture prediction = getMostFrequentGesture(historyEntry);

        secondLastOpponentMove = lastOpponentMove;
        secondLastResult = lastResult;

        return prediction.theWinning();

    }

    private void updateHistory(Gesture lastOpponentMove) {
        if (secondLastOpponentMove != null && secondLastResult != null && lastOpponentMove != null) {
            ImmutablePair<Gesture, Result> secondLastKey = new ImmutablePair<Gesture, Result>(secondLastOpponentMove, secondLastResult);
            Integer times = history.get(secondLastKey).get(lastOpponentMove);
            history.get(secondLastKey).put(lastOpponentMove, times + 1);
        }
    }

    private Gesture getMostFrequentGesture(Map<Gesture, Integer> historyEntry) {
        Gesture prediction = null;
        Integer max = null;
        if (historyEntry != null) {
            for (Map.Entry<Gesture, Integer> entry : historyEntry.entrySet()) {
                Gesture gesture = entry.getKey();
                Integer value = entry.getValue();

                if (max == null || value > max) {
                    prediction = gesture;
                    max = value;
                }

            }
        } else {
            Gesture[] gestures = Gesture.values();
            return gestures[random.nextInt(gestures.length)];
        }
        return prediction;
    }

    @Override
    public void reset() {
        initHistory();
        secondLastOpponentMove = null;
        secondLastResult = null;
    }
}
