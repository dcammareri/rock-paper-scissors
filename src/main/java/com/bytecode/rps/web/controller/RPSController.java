package com.bytecode.rps.web.controller;

import com.bytecode.rps.ai.AI;
import com.bytecode.rps.engine.GameEngine;
import com.bytecode.rps.web.bean.GameBean;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.io.Serializable;

import static com.bytecode.rps.engine.GameEngine.Gesture;

/**
 * Controller that exposes ajax calls to playing rock paper scissors against an AI.
 */
@Controller
@RequestMapping("/")
@Scope("session")
public class RPSController implements Serializable {

    private static final Logger log = LoggerFactory.getLogger(RPSController.class);

    @Autowired
    protected GameBean game;

    @Autowired
    protected AI ai;

    @RequestMapping("/play")
    public
    @ResponseBody
    GameBean play(String gesture, HttpServletResponse response) {
        log.debug("Play request received, gesture is: {}", gesture);
        //check for null
        if (StringUtils.isEmpty(gesture)) {
            log.debug("empty gesture provided, returning 400");
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return null;
        }

        Gesture playerGesture;
        try {
            playerGesture = Gesture.valueOf(gesture.toUpperCase());
        } catch (IllegalArgumentException e) { // check for wrong parameter
            log.debug("Unable to understand gesture value: {}", gesture);
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return null;
        }

        Gesture aiGesture = ai.nextMove(game.getLastPlayer1Gesture(), game.getLastResult());
        log.debug("AI choose its move: {}", aiGesture);

        GameEngine.Result result = GameEngine.play(playerGesture, aiGesture);
        log.info("Game played: player chose {}, AI chose {}, result is {} ", playerGesture, aiGesture, result);

        game.setLastPlayer1Gesture(playerGesture);
        game.setLastPlayer2Gesture(aiGesture);
        game.setLastResult(result);

        switch (result) {
            case PLAYER1WIN:
                game.incrementPlayer1Wins();
                break;
            case TIE:
                game.incrementTies();
                break;
            case PLAYER2WIN:
                game.incrementPlayer2Wins();
                break;
            default:
                log.warn("Gesture not handled: {}", result);
                break;
        }

        return game;
    }

    @RequestMapping("/reset")
    public
    @ResponseBody
    GameBean reset() {
        game.reset();
        ai.reset(); // to do or not to do?
        return game;
    }

    @RequestMapping("/current")
    public
    @ResponseBody
    GameBean current() {
        return game;
    }


}
