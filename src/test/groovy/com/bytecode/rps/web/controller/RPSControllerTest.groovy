package com.bytecode.rps.web.controller

import com.bytecode.rps.ai.AI
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import spock.lang.Specification

import static com.bytecode.rps.engine.GameEngine.Gesture.ROCK
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

/**
 * Created by daniele on 29/06/14.
 */

@ContextConfiguration(locations = ["file:src/main/webapp/WEB-INF/dispatcher-servlet.xml", "classpath:test-application-context.xml"])
@ActiveProfiles(profiles = "dummy-ai")
class RPSControllerTest extends Specification {

    @Autowired
    private RPSController rpsController

    MockMvc mockMvc

    void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(rpsController).build()
    }

    @DirtiesContext
    def "call to gestures with correct gestures should update return 200"() {
        when:
        def response = mockMvc.perform(get("/play?gesture=$gesture"))

        then:
        response
                .andExpect(status().isOk())
                .andExpect(jsonPath("player1Wins").exists())
                .andExpect(jsonPath("player2Wins").exists())
                .andExpect(jsonPath("ties").exists())
                .andExpect(jsonPath("lastPlayer1Gesture").exists())
                .andExpect(jsonPath("lastPlayer1Gesture").exists())
                .andExpect(jsonPath("lastResult").exists())

        where:
        gesture << ['rock', 'paper', 'scissors']
    }

    @DirtiesContext
    def "call to gestures with wrong parameter should return 400 Bad Request"() {
        when:
        def response = mockMvc.perform(get("/play?gesture=wrong"))

        then:
        response.andExpect(status().is(400))

    }

    @DirtiesContext
    def "call to gestures without gesture parameter should return 400 Bad Request"() {
        when:
        def response = mockMvc.perform(get("/play"))

        then:
        response.andExpect(status().is(400))
    }

    @DirtiesContext
    def "call to gestures should update the game correctly"() {
        given:
        rpsController.ai = Stub(AI)
        rpsController.ai.nextMove(_) >> ROCK

        when:
        def response = mockMvc.perform(get("/play?gesture=paper"))

        then:
        response
                .andExpect(jsonPath("player1Wins").value(1))
                .andExpect(jsonPath("player2Wins").value(0))
                .andExpect(jsonPath("ties").value(0))
                .andExpect(jsonPath("lastPlayer1Gesture").value("PAPER"))
                .andExpect(jsonPath("lastPlayer2Gesture").value("ROCK"))
                .andExpect(jsonPath("lastResult").value("PLAYER1WIN"))

        and:

        when:
        response = mockMvc.perform(get("/play?gesture=rock"))

        then:
        response
                .andExpect(jsonPath("player1Wins").value(1))
                .andExpect(jsonPath("player2Wins").value(0))
                .andExpect(jsonPath("ties").value(1))
                .andExpect(jsonPath("lastPlayer1Gesture").value("ROCK"))
                .andExpect(jsonPath("lastPlayer2Gesture").value("ROCK"))
                .andExpect(jsonPath("lastResult").value("TIE"))

        and:

        when:
        response = mockMvc.perform(get("/play?gesture=scissors"))

        then:
        response
                .andExpect(jsonPath("player1Wins").value(1))
                .andExpect(jsonPath("player2Wins").value(1))
                .andExpect(jsonPath("ties").value(1))
                .andExpect(jsonPath("lastPlayer1Gesture").value("SCISSORS"))
                .andExpect(jsonPath("lastPlayer2Gesture").value("ROCK"))
                .andExpect(jsonPath("lastResult").value("PLAYER2WIN"))

    }

    @DirtiesContext
    void "call to reset should reset the game"() {
        given:
        rpsController.ai = Stub(AI)
        rpsController.ai.nextMove(_) >> ROCK
        mockMvc.perform(get("/play?gesture=rock"))
        mockMvc.perform(get("/play?gesture=paper"))
        mockMvc.perform(get("/play?gesture=scissors"))


        when:
        def response = mockMvc.perform(get("/reset"))

        then:
        response.andExpect(jsonPath("player1Wins").value(0))
                .andExpect(jsonPath("player2Wins").value(0))
                .andExpect(jsonPath("ties").value(0))
                .andExpect(jsonPath("lastPlayer1Gesture").doesNotExist())
                .andExpect(jsonPath("lastPlayer2Gesture").doesNotExist())
                .andExpect(jsonPath("lastResult").doesNotExist())
    }

    @DirtiesContext
    void "call to current should return current game data"() {
        given:
        rpsController.ai = Stub(AI)
        rpsController.ai.nextMove(_) >> ROCK

        when:
        def response = mockMvc.perform(get("/current"))

        then:
        response
                .andExpect(jsonPath("player1Wins").value(0))
                .andExpect(jsonPath("player2Wins").value(0))
                .andExpect(jsonPath("ties").value(0))
                .andExpect(jsonPath("lastPlayer1Gesture").doesNotExist())
                .andExpect(jsonPath("lastPlayer2Gesture").doesNotExist())
                .andExpect(jsonPath("lastResult").doesNotExist())
        and:
        mockMvc.perform(get("/play?gesture=rock"))
        mockMvc.perform(get("/play?gesture=scissors"))
        mockMvc.perform(get("/play?gesture=paper"))

        when:
        response = mockMvc.perform(get("/current"))

        then:
        response
                .andExpect(jsonPath("player1Wins").value(1))
                .andExpect(jsonPath("player2Wins").value(1))
                .andExpect(jsonPath("ties").value(1))
                .andExpect(jsonPath("lastPlayer1Gesture").value("PAPER"))
                .andExpect(jsonPath("lastPlayer2Gesture").value("ROCK"))
                .andExpect(jsonPath("lastResult").value("PLAYER1WIN"))
    }

}
