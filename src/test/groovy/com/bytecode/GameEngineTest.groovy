package com.bytecode

import com.bytecode.rps.engine.GameEngine

import static com.bytecode.rps.engine.GameEngine.Gesture.*
import static com.bytecode.rps.engine.GameEngine.Result.*

/**
 * Created by daniele on 27/06/14.
 */
class GameEngineTest extends spock.lang.Specification {
    def "All combinations should be correct "() {
        expect:
        GameEngine.play(a, b) == c

        where:
        a        | b        || c
        ROCK     | ROCK     || TIE
        ROCK     | PAPER    || PLAYER2WIN
        ROCK     | SCISSORS || PLAYER1WIN
        PAPER    | ROCK     || PLAYER1WIN
        PAPER    | PAPER    || TIE
        PAPER    | SCISSORS || PLAYER2WIN
        SCISSORS | ROCK     || PLAYER2WIN
        SCISSORS | PAPER    || PLAYER1WIN
        SCISSORS | SCISSORS || TIE

    }

    def "When one or both inputs are null, should throw IllegalArgumentException"() {
        when:
        GameEngine.play(a, b)

        then:
        thrown IllegalArgumentException

        where:
        a    | b
        null | ROCK
        ROCK | null
        null | null
    }
}
