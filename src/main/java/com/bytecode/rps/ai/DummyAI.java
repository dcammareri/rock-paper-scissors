package com.bytecode.rps.ai;

import com.bytecode.rps.engine.GameEngine;

import java.util.Random;

import static com.bytecode.rps.engine.GameEngine.Gesture;

/**
 * A simple AI that pick out always the same gesture
 */
public class DummyAI implements AI {

    /**
     * The gesture to be picked out every time
     */
    private Gesture gesture;

    @Override
    public Gesture nextMove(Gesture lastOpponentMove, GameEngine.Result lastResult) {
        if (gesture == null) {
            Gesture[] gestures = Gesture.values();
            int random = new Random(System.currentTimeMillis()).nextInt(gestures.length);
            gesture = gestures[random];
        }
        return gesture;
    }

    @Override
    public void reset() {
        gesture = null;
    }
}
