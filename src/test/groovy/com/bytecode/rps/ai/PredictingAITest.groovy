package com.bytecode.rps.ai

import spock.lang.Specification

import static com.bytecode.rps.engine.GameEngine.Gesture.*
import static com.bytecode.rps.engine.GameEngine.Result.PLAYER2WIN
import static com.bytecode.rps.engine.GameEngine.Result.PLAYER1WIN

/**
 * Created by daniele on 29/06/14.
 */
class PredictingAITest extends Specification {

    def "After a player played 3 moves, it should predict correctly"() {
        given:
        PredictingAI ai = new PredictingAI()

        when:
        ai.nextMove(null,null)
        ai.nextMove(ROCK,PLAYER1WIN)
        ai.nextMove(SCISSORS,PLAYER1WIN) //after a [ROCK,PLAYER1WIN], he played scissors


        then:
        ai.nextMove(ROCK,PLAYER1WIN) == ROCK //Expects the player to play scissors, so play ROCK

        and:

        when:
        ai.nextMove(ROCK,PLAYER1WIN)
        ai.nextMove(PAPER,PLAYER2WIN)
        ai.nextMove(ROCK,PLAYER1WIN)
        ai.nextMove(PAPER,PLAYER1WIN) //now, he played 2 times PAPER after [ROCK,PLAYER1WIN]

        then:
        ai.nextMove(ROCK,PLAYER1WIN) == SCISSORS //Expects the player to play paper, so play SCISSORS
    }

    def "after some move has been registered, a call with null values should work"() {
        given:
        PredictingAI ai = new PredictingAI()

        when:
        ai.nextMove(null,null)
        ai.nextMove(ROCK,PLAYER1WIN)

        then:
        ai.nextMove(null,null) in [ROCK,PAPER,SCISSORS]
    }

    def "reset should delete historical data"() {
        given:
        PredictingAI ai = new PredictingAI()

        when:
        ai.nextMove(null,null)
        ai.nextMove(ROCK,PLAYER1WIN)
        ai.nextMove(SCISSORS,PLAYER1WIN)
        ai.nextMove(ROCK,PLAYER1WIN)
        ai.nextMove(SCISSORS,PLAYER1WIN) // scissors played two times after [ROCK,PLAYER1WIN]
        ai.reset()
        ai.nextMove(ROCK,PLAYER1WIN)
        ai.nextMove(PAPER,PLAYER1WIN) //paper played one time after [ROCK, PLAYER1WIN], but after reset

        then:
        ai.nextMove(ROCK,PLAYER1WIN) == SCISSORS //I expect to player to play paper, so play SCISSORS
    }
}
