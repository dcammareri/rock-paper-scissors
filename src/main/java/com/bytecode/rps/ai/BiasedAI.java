package com.bytecode.rps.ai;

import com.bytecode.rps.engine.GameEngine;
import com.bytecode.rps.util.ArrayUtils;

import java.util.Random;

import static com.bytecode.rps.engine.GameEngine.Gesture;


/**
 * A simple AI Implementation the prefer a particular move.
 * <p/>
 * The preferred gesture is 50% likely to be picked out, the other ones 25%
 */
public class BiasedAI implements AI {

    /**
     * A random shuffling of gestures. The first element is the preferred one.
     */
    private Gesture[] gestures;

    private Random random = new Random(System.currentTimeMillis());

    public BiasedAI() {
        gestures = Gesture.values();
        ArrayUtils.shuffleArray(gestures);
    }

    @Override
    public Gesture nextMove(Gesture lastOpponentMove, GameEngine.Result lastResult) {
        // I take one additional randomIndex choice.
        int randomIndex = random.nextInt(gestures.length + 1);
        //by making this mod, the additional choice is equal to 0
        randomIndex = randomIndex % (gestures.length - 1);
        return gestures[randomIndex];
    }

    @Override
    public void reset() {
        ArrayUtils.shuffleArray(gestures);
    }

}
