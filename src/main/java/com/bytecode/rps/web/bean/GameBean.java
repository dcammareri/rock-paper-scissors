package com.bytecode.rps.web.bean;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.Serializable;

import static com.bytecode.rps.engine.GameEngine.Gesture;
import static com.bytecode.rps.engine.GameEngine.Result;

/**
 * Holds data for a match of a user against the AI.
 */

@Component
@Scope("session")
public class GameBean implements Serializable {

    private Integer player1Wins = 0;
    private Gesture lastPlayer1Gesture;
    private Integer player2Wins = 0;
    private Gesture lastPlayer2Gesture;

    private Integer ties = 0;
    private Result lastResult;

    public void reset() {
        player1Wins = 0;
        lastPlayer1Gesture = null;
        player2Wins = 0;
        lastPlayer2Gesture = null;
        ties = 0;
        lastResult = null;
    }

    public void incrementPlayer1Wins() {
        player1Wins++;
    }

    public void incrementPlayer2Wins() {
        player2Wins++;
    }

    public void incrementTies() {
        ties++;
    }

    public Gesture getLastPlayer1Gesture() {
        return lastPlayer1Gesture;
    }

    public void setLastPlayer1Gesture(Gesture lastPlayer1Gesture) {
        this.lastPlayer1Gesture = lastPlayer1Gesture;
    }

    public Gesture getLastPlayer2Gesture() {
        return lastPlayer2Gesture;
    }

    public void setLastPlayer2Gesture(Gesture lastPlayer2Gesture) {
        this.lastPlayer2Gesture = lastPlayer2Gesture;
    }

    public Result getLastResult() {
        return lastResult;
    }

    public void setLastResult(Result lastResult) {
        this.lastResult = lastResult;
    }

    public Integer getPlayer1Wins() {
        return player1Wins;
    }

    public Integer getPlayer2Wins() {
        return player2Wins;
    }

    public Integer getTies() {
        return ties;
    }

}
