package com.bytecode.rps.util;

import com.bytecode.rps.engine.GameEngine;

import java.util.Random;

/**
 * Contains Utility functions for Java arrays (e.g. shuffling)
 */
public abstract class ArrayUtils {

    /**
     * Implementing Fisher–Yates shuffle. Guarantees uniform distributed shuffling in linear time
     * The implementation is taken from The Internet
     *
     * @param ar the array to shuffle (in place)
     */

    public static void shuffleArray(GameEngine.Gesture[] ar) {
        Random rnd = new Random();
        for (int i = ar.length - 1; i > 0; i--) {
            int index = rnd.nextInt(i + 1);
            // Simple swap
            GameEngine.Gesture a = ar[index];
            ar[index] = ar[i];
            ar[i] = a;
        }
    }

}
